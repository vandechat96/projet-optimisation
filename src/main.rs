use crate::meta::annealing::annealing_multithread;
use crate::meta::tabu::{tabu_multithread, test_tabu_multithread};
use crate::utils::{find_solution, load_instances, print_test_result};

mod utils;
mod neighbor;
mod meta;

fn main() {
    //let instances = load_instances();
    //let sols = meta::memetic::test_memetic(&split_number_equally,&shuffle_two_number_equally);
    //print_test_result(&instances,sols)
    //meta::print_results();
    //print_test_result(&instances,test_tabu_multithread());

    let path = "instances/data10.dat";
    find_solution(5, path,"Groupe11-Challenge1.txt",&tabu_multithread)

}
