pub mod tabu;
pub mod annealing;
pub mod memetic;


use crate::meta::annealing::test_annealing;
use crate::meta::memetic::test_memetic;
use crate::meta::tabu::test_tabu;
use crate::neighbor::{shuffle_two_number_equally, shuffle_two_number_randomly, shuffle_two_number_randomly2};
use crate::utils::{load_instances, print_test_result, split_number_equally, split_number_random, split_number_random2};

pub fn print_results(){
    let instances = load_instances();


    println!("{:=<2$} START {:=>2$}","","",30);


    println!("=> TABU EQUAL");
    let mut results = test_tabu(&shuffle_two_number_equally);
    print_test_result(&instances,results);


    println!("=> TABU RANDOM");
    results = test_tabu(&shuffle_two_number_randomly);
    print_test_result(&instances,results);

    println!("=> TABU RANDOM2");
    results = test_tabu(&shuffle_two_number_randomly2);
    print_test_result(&instances,results);

    println!();
    println!("=> ANNEALING EQUAL");
    results = test_annealing(&shuffle_two_number_equally);
    print_test_result(&instances,results);


    println!("=> ANNEALING RANDOM");
    results = test_annealing(&shuffle_two_number_randomly);
    print_test_result(&instances,results);

    println!("=> ANNEALING RANDOM2");
    results = test_annealing(&shuffle_two_number_randomly2);
    print_test_result(&instances,results);


    println!();
    println!("=> MEMETIC EQUAL");
    results = test_memetic(&split_number_equally, &shuffle_two_number_equally);
    print_test_result(&instances,results);


    println!("=> MEMETIC RANDOM");
    results = test_memetic(&split_number_random, &shuffle_two_number_randomly);
    print_test_result(&instances,results);

    println!("=> MEMETIC RANDOM2");
    results = test_memetic(&split_number_random2, &shuffle_two_number_randomly2);
    print_test_result(&instances,results);


    println!("{:=<2$} END {:=>2$}","","",30);
}