use std::f32::consts::E;
use rand::{Rng, thread_rng};
use crate::neighbor::shuffle_two_number_randomly2;
use crate::utils::{Instance, load_instances, read_data, Solution};
use threadpool::ThreadPool;
use std::sync::{Arc, Barrier};
use std::sync::RwLock;


pub fn annealing(instance:&Instance, t0:f32, tf:f32, bearing:u32, coef:f32, neighbor_method: &dyn Fn(&Solution)->Solution) -> Solution{
    let mut previous_sol = instance.gen_base();
    let mut best_sol = previous_sol.clone();

    let mut best_cost = best_sol.get_cost(instance);
    let mut previous_cost = previous_sol.get_cost(instance);

    let mut tc = t0;
    let mut rng = thread_rng();

    while tc > tf {
        for _i in 0..bearing{
            let neighbor:Solution = neighbor_method(&previous_sol);
            let cost = neighbor.get_cost(instance);

            if cost < best_cost{
                best_sol = neighbor.clone();
                best_cost = cost;
            }

            let delta_e = cost - previous_cost;
            let rnd_nbr :f32 = rng.gen_range(0.0..1.0);
            if delta_e <= 0 || rnd_nbr < (E.powf(-delta_e as f32 / tc)){
                previous_sol = neighbor;
                previous_cost = cost;
            }
        }
        tc = tc *coef;
    }
    best_sol
}

/**
test the method on all instances
*/
pub fn test_annealing(neighbor_method: &dyn Fn(&Solution)->Solution)->Vec<Solution>{
    let instances = load_instances();
    let mut results = Vec::with_capacity(10);

    for instance in instances {
        let sol = annealing(&instance,1000.0,0.0001, 1000, 0.8, neighbor_method);
        results.push(sol);
    }
    results
}

pub fn annealing_multithread(file:String) -> Solution{
    let n_workers = num_cpus::get();
    let n_jobs = num_cpus::get();
    let pool = ThreadPool::new(n_workers);
    let results = Arc::new(RwLock::new(Vec::<Solution>::new()));
    let instance = &read_data(file.as_str());

    assert!(n_jobs <= n_workers, "too many jobs, will deadlock");

    // create a barrier that waits for all jobs plus the starter thread
    let barrier = Arc::new(Barrier::new(n_jobs + 1));
    for _ in 0..n_jobs {
        let barrier = barrier.clone();
        let arc = results.clone();
        let file = file.clone();

        pool.execute(move || {
            // do the heavy work
            let instance =read_data(file.as_str());
            let sol = annealing(&instance, 10000.0,0.00001, 10000, 0.9, &shuffle_two_number_randomly2);

            arc.try_write().unwrap().push(sol);

            // then wait for the other threads
            barrier.wait();
        });
    }

    // wait for the threads to finish the work
    barrier.wait();
    let tab = results.try_read().unwrap();
    let min = tab.iter().reduce(|x, x1| {
        match x.get_cost(instance) < x1.get_cost(instance) {
            true => x,
            false => x1
        }
    });

    min.unwrap().clone()
}
pub fn test_annealing_multithread() ->Vec<Solution>{

    let instances =load_instances();

    let mut results_total = Vec::with_capacity(10);

    for i in 0..10{    // create at least as many workers as jobs or you will deadlock yourself
        let sol = annealing_multithread(format!("instances/data{}.dat",i));
        results_total.push(sol.clone());
    }
    results_total
}
#[cfg(test)]
mod test {
    use crate::meta::annealing::annealing;
    use crate::neighbor::shuffle_two_number_equally;
    use super::{load_instances};

    #[test]
    fn verification() {
        let instances = load_instances();

        for instance in instances {
            let sol = annealing(&instance,100.0,0.001, 100, 0.7, &shuffle_two_number_equally);
            assert_ne!(-1, sol.get_cost(&instance))
        }

    }
}