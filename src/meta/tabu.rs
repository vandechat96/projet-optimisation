use crate::neighbor::{shuffle_two_number_equally, shuffle_two_number_randomly2};
use crate::utils::{Instance, load_instances, read_data, Solution};
use threadpool::ThreadPool;
use std::sync::{Arc, Barrier};
use std::sync::RwLock;

pub fn tabu(instance: &Instance, memory:usize, iterations:u32, neighbor_method: &dyn Fn(&Solution)->Solution) -> Solution{
    let s0 = instance.gen_base();
    let mut s_best = s0.clone();
    let mut best_cost = s0.get_cost(instance);

    let mut tabu_list:Vec<Solution> = Vec::with_capacity(memory);
    //tabu_list.push(s0);

    for _i in 0..iterations {
        let  neighbor:Solution = neighbor_method(&s_best);
        let  cost = neighbor.get_cost(instance);

        if !tabu_list.contains(&&neighbor){
            if cost < best_cost{
                best_cost = cost;
                s_best = neighbor.clone();
            }
            tabu_list.push(s_best.clone());

            if tabu_list.len() > memory{
                tabu_list.remove(0);
            }
        }
    }
    s_best
}


/**
test the method on all instances
 */
pub fn test_tabu(neighbor_method: &dyn Fn(&Solution)->Solution) ->Vec<Solution>{
    let instances = load_instances();
    let mut results = Vec::with_capacity(10);

    for instance in instances {
        let sol = tabu(&instance,20,200000, &neighbor_method);
        results.push(sol);
    }
    results
}

pub fn tabu_multithread(file:String) -> Solution{
    // create at least as many workers as jobs or you will deadlock yourself
    let n_workers = num_cpus::get();
    let n_jobs = num_cpus::get();
    let pool = ThreadPool::new(n_workers);

    let results = Arc::new(RwLock::new(Vec::<Solution>::new()));
    let instance = &read_data(file.as_str());


    assert!(n_jobs <= n_workers, "too many jobs, will deadlock");

    // create a barrier that waits for all jobs plus the starter thread
    let barrier = Arc::new(Barrier::new(n_jobs + 1));
    for _ in 0..n_jobs {
        let barrier = barrier.clone();
        let arc = results.clone();
        let file = file.clone();

        pool.execute(move || {
            // do the heavy work
            let instance = read_data(file.as_str());
            let sol = tabu(&instance, 20, 300000, &shuffle_two_number_randomly2);

            arc.try_write().unwrap().push(sol);

            // then wait for the other threads
            barrier.wait();
        });
    }

    // wait for the threads to finish the work
    barrier.wait();
    let tab = results.try_read().unwrap();
    let min = tab.iter().reduce(|x, x1| {
        match x.get_cost(instance) < x1.get_cost(instance) {
            true => x,
            false => x1
        }
    });

   min.unwrap().clone()
}

pub fn test_tabu_multithread() ->Vec<Solution>{

    let mut results_total = Vec::with_capacity(10);

    for i in 1..11{

        let sol = tabu_multithread(format!("instances/data{}.dat",i));
        results_total.push(sol.clone());
    }
    results_total
}
#[cfg(test)]
mod test {
    use super::tabu;
    use crate::neighbor::shuffle_two_number_randomly;
    use super::{load_instances};

    #[test]
    fn verification() {
        let instances = load_instances();

        for instance in instances {
            let sol = tabu(&instance, 8, 1000, &shuffle_two_number_randomly);
            assert_ne!(-1, sol.get_cost(&instance))
        }

    }
}