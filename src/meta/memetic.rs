use std::cmp::max;
use std::f32::consts::E;
use rand::{Rng, thread_rng};
use crate::utils::{Instance, load_instances, Solution};


/**
Generate a random population randomly using neighbor_method
*/
fn generate_population(instance:&Instance, size:usize, randomization:usize, neighbor_method: &dyn Fn(&Solution)->Solution)->Vec<Solution>{
    let mut population = Vec::with_capacity(size);
    for _i in 0..size {
        let mut baby =  instance.gen_base();
        for _j in 0..randomization{
            baby = neighbor_method(&baby);
        }
        population.push(baby)
    }
    population
}

struct PopEvaluation{
    costs:Vec<i32>,
    minimum:i32,
    maximum:i32,
    best:Solution
}

fn evaluate_population(instance:&Instance, population:&Vec<Solution>)->PopEvaluation{
    let mut costs = Vec::with_capacity(population.len());
    let mut minimum =  population[0].get_cost(instance);
    let mut maximum = minimum;
    let mut best= &population[0];

    for people in population {
        let cost = people.get_cost(instance);
        costs.push(cost);
        if cost < minimum {
            minimum = cost;
            best = people
        }
        maximum = max(maximum,cost)
    }
    if maximum == minimum {
        maximum+=1;
    }
    PopEvaluation{costs, minimum, maximum, best: best.clone()}
}

struct Couple{
    p1:Solution,
    p2:Solution
}

/**
*/
pub fn evolve(instance:&Instance, population_size:usize, number_of_generation:usize, number_mutation:usize, split_method:&dyn Fn(u32, u32) ->Vec<u32>, mutation_method: &dyn Fn(&Solution) -> Solution) ->Solution{
    let mut population = generate_population(instance, population_size, 3, mutation_method);
    let mut best = population[0].clone();
    let mut best_cost = best.get_cost(instance);
    let mut rng = thread_rng();

    let mut tc = number_of_generation as f32;
    //for _gen_nbr in 0..number_of_generation {
    while tc > 0.0000001 {
        let eval = evaluate_population(instance,&population);

        // Generate couple of parents
        let mut parents = Vec::with_capacity(population.len() /2);

        while parents.len() < population.len() / 2 {
            let mut parent1 = Solution { slices: vec![] };
            let mut single = true;

            for indice in 0..population.len() {

                let probability = (eval.costs[indice] - eval.minimum)as f32 /(eval.maximum-eval.minimum) as f32;
                let random:f32 = rng.gen_range(0.0..1.0);

                if probability < random{
                    if single{
                        parent1 = population[indice].clone();
                        single = false;
                    } else {
                        parents.push(Couple{p1:parent1.clone(),p2:population[indice].clone()});
                        single = true;
                    }
                }
                if parents.len() >= population.len()/2 {
                    break;
                }
            }
        }


        // Generate and mutate children
        let mut children = Vec::with_capacity(parents.len()/2);
        for couple in parents {
            let parent1 = couple.p1;
            let parent2 = couple.p2;

            let mut child = parent1.clone();
            if rng.gen_bool(0.7){
                let nbr1 = rng.gen_range(0..parent1.slices.len());
                let nbr2 = rng.gen_range(0..parent1.slices.len());

                child.slices[nbr1] = split_method(child.slices[nbr1].iter().sum(), parent2.slices[nbr1].len() as u32);
                child.slices[nbr2] = split_method(child.slices[nbr2].iter().sum(), parent2.slices[nbr2].len() as u32);

                let mut compensation = parent1.space_occupied() as i16 - child.space_occupied() as i16;
                let derivation:i16 = match compensation > 0 {
                    true => 1,
                    false => -1
                };
                while compensation != 0{
                    for i in 0..parent1.slices.len(){
                        if ![nbr1, nbr2].contains(&i){
                            let new_div = child.slices[i].len() as i16 + derivation;
                            if new_div > 0{
                                child.slices[i] = split_method(child.slices[i].iter().sum(), new_div as u32);
                                compensation -= derivation;
                            }
                        }
                        if compensation == 0 {
                            break;
                        }
                    }
                }

                let old_child = child.clone();
                let mut cost = child.get_cost(instance);
                if rng.gen_bool(0.5){
                    for _i in 0..number_mutation{
                        let mutant:Solution = mutation_method(&old_child);
                        let new_cost = mutant.get_cost(instance);
                        if new_cost < cost {
                            child = mutant;
                            cost = new_cost;
                        }
                    }
                }
                children.push(child)
            }
        }
        population.append(&mut children);
        let eval = evaluate_population(instance, &population);

        let mut new_population = Vec::with_capacity(population_size);
        let mut i = 0;
        while new_population.len() < population_size {

            let indice = rng.gen_range(0..population.len());

            //let probability = (eval.costs[indice] - eval.minimum)as f32 /(eval.maximum-eval.minimum) as f32;
            let random:f32 = rng.gen_range(0.0..1.0);

            if i>= 10000 {
                break
            }
            i+=1;
            let delta_e = eval.costs[indice] - (eval.minimum)*12/10;
            if delta_e <= 0 || random < (E.powf(-delta_e as f32 / tc )){
                if !new_population.contains(&population[indice]) {
                    new_population.push(population[indice].clone());
                    population.remove(indice);
                }
            }

        }

        population = new_population;
        if eval.minimum < best_cost {
            best_cost = eval.minimum;
            best = eval.best
        }
        tc = tc *0.9;
    }
    best
}

/**
test the method on all instances
 */
pub fn test_memetic(split_method:&dyn Fn(u32, u32) ->Vec<u32>, neighbor_method: &dyn Fn(&Solution)->Solution)->Vec<Solution>{
    let instances = load_instances();
    let mut results = Vec::with_capacity(10);

    for instance in instances {
        let sol = evolve(&instance,100, 10000, 20,&split_method,&neighbor_method);
        results.push(sol);
    }
    results
}



#[cfg(test)]
mod test {
    use crate::meta::memetic::evolve;
    use crate::neighbor::shuffle_two_number_randomly;
    use crate::utils::{load_instances, split_number_equally};

    #[test]
    fn verification() {
        let instances = load_instances();

        for instance in instances {
            let sol = evolve(&instance, 50, 100, 2,&split_number_equally,&shuffle_two_number_randomly);
            assert_ne!(-1, sol.get_cost(&instance))
        }

    }
}