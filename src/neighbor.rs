use rand::{Rng, thread_rng};
use crate::utils::{Solution, split_number_equally, split_number_random, split_number_random2};

/**
Take two random numbers and splits them as specified with split_method
The split conserves the space occupied by the numbers
*/
pub fn shuffle_two_number(solution:&Solution, split_method: &dyn Fn(u32, u32) -> Vec<u32>)->Solution{
    let mut sol = solution.slices.clone();
    let length = sol.len();
    let mut rng = thread_rng();

    let indice1 = rng.gen_range(0..length);
    let division1 = &sol[indice1];
    let mut indice2 = rng.gen_range(0..length);
    while indice2 == indice1 {
        indice2 = rng.gen_range(0..length);
    }
    let division2 = &sol[indice2];

    let nbr_box = division1.len() + division2.len();
    let nbr1 = division1.iter().sum();
    let nbr2 = division2.iter().sum();


    let nbr_division = rng.gen_range(1..nbr_box) as u32;

    sol[indice1] = split_method(nbr1, nbr_division);

    sol[indice2] = split_method(nbr2, nbr_box as u32 - nbr_division);

    Solution{slices:sol}
}

pub fn shuffle_two_number_randomly(solution:&Solution)-> Solution{
    shuffle_two_number(solution,&split_number_random)
}

pub fn shuffle_two_number_randomly2(solution:&Solution)-> Solution{
    shuffle_two_number(solution,&split_number_random2)
}


pub fn shuffle_two_number_equally(solution:&Solution) -> Solution{
    shuffle_two_number(solution,&split_number_equally)
}

#[cfg(test)]
mod test {
    use crate::neighbor::{shuffle_two_number_equally, shuffle_two_number_randomly};
    use crate::utils::{load_instances};

    #[test]
    fn shuffle_test_equally() {
        let instance = &load_instances()[3];
        let sol = instance.gen_base();

        let shuffle = shuffle_two_number_equally(&sol);
        assert_ne!(-1, shuffle.get_cost(instance))
    }

    #[test]
    fn shuffle_test_random() {
        let instance = &load_instances()[3];
        let sol = instance.gen_base();

        let shuffle = shuffle_two_number_randomly(&sol);
        assert_ne!(-1, shuffle.get_cost(instance))
    }
}