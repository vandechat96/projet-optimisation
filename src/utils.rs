use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::{cmp, iter};
use rand::{Rng, thread_rng};
use rand::distributions::{Distribution, Uniform};


/**
Represent a solution to a problem instance
It contains a list of the number spliced accordingly ans some helper functions
*/
#[derive(Debug, Clone)]
pub struct Solution{
    pub slices:Vec<Vec<u32>>
}

impl PartialEq for Solution {
    fn eq(&self, other: &Self) -> bool {
        vec_of_vec_compare(&self.slices, &other.slices)
    }
}

fn vec_compare(va: &Vec<u32>, vb: &Vec<u32>) -> bool {
    (va.len() == vb.len()) &&  // zip stops at the shortest
        va.iter()
            .zip(vb)
            .all(|(a,b)| a == b)
}

fn vec_of_vec_compare(va: &Vec<Vec<u32>>, vb: &Vec<Vec<u32>>) -> bool {
    (va.len() == vb.len()) &&  // zip stops at the shortest
        va.iter()
            .zip(vb)
            .all(|(a,b)| vec_compare(a,b))
}
impl Eq for Solution {}

impl Solution {

    /**
    Flatten the list and sort it for easier calculations
    */
    fn flatten(&self)->Vec<&u32>{
        let mut list = Vec::new();

        for slice in &self.slices {
            for nbr in slice {
                list.push(nbr);
            }
        }
        list.sort_by(|a, b| b.cmp(a));
        list
    }
    /**
    Space occupied by splices of number
    */
    pub fn space_occupied(&self) -> usize{
        self.flatten().len()
    }

    /**
    Calculate the cost of the solution for a given instance
    Return -1 if the solution is not acceptable
    */
    pub fn get_cost(&self, instance:&Instance)-> i32{
        let list = self.flatten();

        if list.len() != instance.box_nbr * instance.box_size {
            -1
        } else {
            let mut cost = 0;
            // add the maximum for each box to the cost
            for i in 0..(list.len()/instance.box_size ) {
                cost+=list[i*instance.box_size];
            }
            cost as i32
        }
    }

    /**
    Create a string with the required format
    */
    pub fn export_string(&self, instance:&Instance) ->String{
        let mut string = String::new();

        let flat = self.flatten();
        let mut cost = 0;
        let mut maxima:Vec<u32> = Vec::with_capacity(instance.box_nbr);

        for i in 0..(flat.len()/instance.box_size ) {
            let nbr = flat[i*instance.box_size];
            maxima.push(*nbr);
            cost+=nbr;
        }

        let max = cmp::max(flat.iter().max().unwrap().to_string().len(), cost.to_string().len());
        let max_s = self.slices.iter().map(|x| {x.len()}).max().unwrap().to_string().len();

        for (iter, slice) in self.slices.iter().enumerate(){
            let nbr:u32 = slice.iter().sum();
            string.push_str(&*format!("{iter:<5}{number:<max$}  {splices:<max_s$} ", iter = iter + 1, number = nbr, max = max, splices = slice.len(), max_s = max_s));
            for number in slice {
                string.push_str(&*format!("{:<1$} ", number, max));
            }
            string.push_str("\n");
        }

        for (iter, maximum) in maxima.iter().enumerate() {
            let  box_str= format!("B{}",iter+1);
            string.push_str(&*format!("{:<1$}{2}\n",box_str,5,maximum));
        }

        string.push_str(&*format!("COST {:<1$}", cost, max));
        string
    }

    /**
    Write the solution to a file with a given name
    */
    pub fn export_to_disk(&self, instance:&Instance, name:&str){
        let mut file = BufWriter::new(File::create(name).unwrap());
        file.write_all(self.export_string(instance).as_ref()).unwrap();
    }
}

/**
Represents the problem instance with the different parameters and help function
*/
#[derive(Debug)]
pub struct Instance{
    pub length:usize,
    pub box_size:usize,
    pub box_nbr:usize,
    pub nbr_list:Vec<u32>
}

impl Instance {
    /**
    Generate a simple solution for the problem
    */
    pub fn gen_base(&self)-> Solution{
        let mut sorted_data = self.nbr_list.clone();
        sorted_data.sort_by(|a, b| b.cmp(a));
        let space = self.box_nbr * self.box_size;

        // calculate the maximum division possible for all the number
        let mut div = 1;
        while (div + 1)*self.length <= space {
            div +=1;
        };

        // space that remain after all number are spliced
        let left = space - div * self.length;

        let mut slices: Vec<Vec<u32>> = iter::repeat_with(|| Vec::new())
            .take(self.length)
            .collect();

        // augment div number by one for the first numbers
        div +=1;
        for i in 0..sorted_data.len(){
            // when the first numbers are divided correctly diminish div
            if i == left {
                div -= 1;
            }
            // divide the number in div slices and add the remaining to the first
            let slice = sorted_data[i] / div as u32;
            let remaining = sorted_data[i] % div as u32;
            slices[i].push(slice + remaining);
            for _j in 1..div{
                slices[i].push(slice);
            }
        }
        Solution{ slices }
    }
}
/**
Read instance data in the specified file
*/
pub fn read_data(file:&str)->Instance{
    // Open the file in read-only mode (ignoring errors).
    let file = File::open(file).unwrap();
    let reader = BufReader::new(file);

    let mut length = 0;
    let mut box_size = 0;
    let mut box_nbr = 0;
    let mut nbr_list = Vec::new();
    // Read the file line by line using the lines() iterator from std::io::BufRead.
    for (index, line) in reader.lines().enumerate() {
        let line = line.unwrap(); // Ignore errors.
        match index {
            0 => length =  line.parse().unwrap(),
            1 => box_size =  line.parse().unwrap(),
            2 => box_nbr =  line.parse().unwrap(),
            _ => nbr_list.push( line.parse().unwrap()),
        }
    }
    Instance{length,box_size,box_nbr,nbr_list}
}

/**
Read the instances in the files and return a vector of problems
*/
pub fn load_instances() -> Vec<Instance>{
    let mut instances = Vec::new();
    for i in 1..11 {
        let filename = format!("instances/data{}.dat",i) ;
        instances.push(read_data(filename.as_str()));
    }
   instances
}

/**
Split a number evenly in split_nbr part
*/
pub fn split_number_equally(number:u32, split_nbr:u32) ->Vec<u32>{
    let mut spliced = Vec::with_capacity(split_nbr as usize);
    let div = number /split_nbr;
    let surplus = number % split_nbr;
    spliced.push(div + (surplus % split_nbr));
    for _n in 0..split_nbr-1{
        spliced.push(div + surplus / split_nbr)
    }
    spliced
}

/**
Split a number randomly in split_nbr part
The randomness correspond to the start of the cut for each iteration
 */
const RANDOMNESS: f32 = 0.0;
pub fn split_number_random(mut number:u32, split_nbr:u32) ->Vec<u32>{
    let mut spliced = Vec::with_capacity(split_nbr as usize);
    let mut rng = thread_rng();

    for _n in 0..split_nbr-1{
        let slice = rng.gen_range(((number as f32 * RANDOMNESS) as u32)..number);
        number -= slice;
        spliced.push(slice)
    }
    spliced.push(number);
    spliced
}

/**
Split a number randomly in split_nbr part
The randomness correspond to the start of the cut for each iteration
 */
pub fn split_number_random2(number:u32, split_nbr:u32) ->Vec<u32>{
    let mut spliced = Vec::with_capacity(split_nbr as usize);
    let mut split = Vec::with_capacity(split_nbr as usize);

    let mut rng = thread_rng();
    let min = (number as f32 * RANDOMNESS/4.0) as u32;
    let between = Uniform::from(min..number-min);

    for _ in 0..split_nbr-1 {
        split.push(between.sample(&mut rng))
    }
    split.append(&mut vec![0,number]);
    split.sort();

    for i in 1..(split_nbr+1) as usize {
        spliced.push(split[i]-split[i-1]);
    }

    spliced
}

/**
 Print the comparaison of the results
 */
pub fn print_test_result(instances:&Vec<Instance>, solutions:Vec<Solution>){
    let prof_costs = vec![5243, 8190, 3897, 9978, 4966, 15030, 7194, 239778, 229428, 226788];

    for (index, sol) in solutions.iter().enumerate() {
        let instance = &instances[index];

        let cost = sol.get_cost(instance) as f32;
        let prof_cost = prof_costs[index] as f32;

        let prc:f32 = 100.0 * (cost - prof_cost) / prof_cost;
        println!("Problem number: {}", index+1);
        println!("Variation related to the 'good' results: {}", prc);

    }
}

pub fn find_solution(n:usize, path:&str,filename:&str, method: &dyn Fn(String)->Solution){
    let instance = &read_data(path);
    let mut res:Vec<Solution> = Vec::with_capacity(n);
    for _ in 0..n {
        res.push(method(String::from(path)))
    }

    let min = res.iter().reduce(|x, x1| {
        match x.get_cost(instance) < x1.get_cost(instance) {
            true => x,
            false => x1
        }
    });

    min.unwrap().export_to_disk(instance,filename);
}

#[cfg(test)]
mod test {
    use crate::utils::{split_number_equally, split_number_random, split_number_random2};
    use super::{load_instances};

    #[test]
    fn basics() {
        let targets = vec![5906, 9735, 4346, 11010, 5784, 15680, 8177, 247372, 246865, 238585];
        let instances = load_instances();
        for (index, instance) in instances.iter().enumerate(){
            let sol = instance.gen_base();
            assert_eq!(targets[index],sol.get_cost(&instance));
            assert_eq!(instance.box_size*instance.box_nbr,sol.space_occupied())
        }
    }

    #[test]
    fn split_method_flat(){
        let result_flat = vec![1434, 1432, 1432, 1432];
        let spliced = split_number_equally(5730, 4);
        assert_eq!(result_flat, spliced);
    }

    #[test]
    fn split_method_random(){
        let spliced = split_number_random(5730, 4);
        assert_eq!(4, spliced.len());
        assert_eq!(5730u32, spliced.iter().sum())
    }

    #[test]
    fn split_method_random2(){
        let spliced = split_number_random2(5730, 4);
        assert_eq!(4, spliced.len());
        assert_eq!(5730u32, spliced.iter().sum())
    }

    #[test]
    fn solution_equality(){
        let instances = load_instances();
        assert_eq!(instances[0].gen_base(),instances[0].gen_base());
        assert_ne!(instances[1].gen_base(),instances[3].gen_base());

        let solutions = vec![instances[1].gen_base(),instances[2].gen_base()];
        assert_eq!(true, solutions.contains(&instances[1].gen_base()));
        assert_ne!(true, solutions.contains(&instances[5].gen_base()))
    }
}
