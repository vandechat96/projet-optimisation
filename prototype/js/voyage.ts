import rand from "./rnd.ts";

let cout:number[][] //= [[-1,4,8,9],[5,-1,4,3],[7,1,-1,4],[9,7,4,-1]]


function gen_cout(length:number,max=10,min=1):number[][]{
    let l:number[][] = []
    for (let i = 0; i < length; i++) {
        let sub = []
        for (let j = 0; j <length; j++) {
            if (i === j) sub.push(-1)
            else sub.push(Math.floor(rand.f64()*max+min))
        }
        l.push(sub)
    }
    return l
}

function eval_path(path:number[], cout:number[][]):number{
    if (path.length !== cout.length) return -1
    let used:number[] = [path[0]]
    let total = 0

    for (let i = 1; i < path.length; i++) {
        const number = path[i]
        if (used.indexOf(number) !== -1) return -1
        else {
            const nbr = used[used.length-1]
            total+= cout[nbr-1][number-1]
            used.push(number)
        }
    }
    const last = used[used.length-1]-1
    const first = used[0]-1
    total+= cout[last][first]
    return total
}

function gen_path(length:number):number[]{
    let path:number[] = []
    while (path.length < length) {
        let nbr = Math.floor(rand.f64()*length+1)
        if (path.indexOf(nbr) === -1) path.push(nbr)
    }
    return path
}




function generate_population(cout:number[][], nbr : number):number[][]{
    let pop:number[][] = []
    for (let i = 0; i < nbr; i++) {
         pop.push(gen_path(cout.length))
    }
    return pop
}

//console.log(generate_population(cout,10))

//let solution = [3,1,2,4]
//console.log(eval_path(solution,cout))

//console.log(gen_cout(10))

interface Parents_interface{
    p1:number[];
    p2:number[];
}

interface solution{
    path:number[]
    val:number
}

cout = gen_cout(20)

let population = generate_population(cout, 30)
let best:solution = {path:population[0], val: eval_path(population[0],cout)}

for (let i = 0; i < 100; i++) {
    let min = eval_path(population[0], cout)
    let max = eval_path(population[0], cout)

    for (const path of population) {
        const val = eval_path(path, cout)
        max = val > max ? val : max
        min = val < min ? val : min
    }
    if (min === max) break

    //console.log(min, max)
    let parents: Parents_interface[] = []
    while (parents.length < population.length / 2) {
        let p1
        let p2
        let ok = false
        for (const path of population) {
            const prob = (eval_path(path,cout)-min)/(max-min)
            const rnd = rand.f64()

            //console.log(prob)

            if (rnd<prob) {

                if (!ok) p1 = path
                else {
                    p2 = path
                    // @ts-ignore
                    parents.push({p1, p2})
                    if (parents.length >= population.length / 2) break
                }
                ok=!ok
            }
        }
        if (parents.length === 0) Deno.exit(3)
    }

    // gen random children
    let children: number[][] = []
    while ( children.length < population.length / 2) {
        const rnd = Math.floor(rand.f64() * population.length / 2)
        let {p1, p2} = parents[rnd]

        let child = []
        for (let j = 0; j < Math.min(p1.length, p2.length); j++) {
            const rnd = rand.u1()
            child.push(rnd === 1 ? p1[j] : p2[j])
        }
        if(eval_path(child,cout) > 0) children.push(child)
    }

    for (let child of children) {
        const rnd = rand.f64()
        if (rnd < 0.2) {
            const rnd1 = Math.floor(rand.f64()*cout.length)
            const rnd2 = Math.floor(rand.f64()*cout.length)
            if (rnd1 !== rnd2) {
                const temp = child[rnd1]
                child[rnd1] = child[rnd2]
                child[rnd2] = temp
            }
        }
    }

    for (const path of children) {
        const val = eval_path(path,cout)
        max = val > max ? val : max
        min = val < min ? val : min
    }



    // chose random new pop
    let new_pop: number[][] = []
    let nbr = population.length

    while (new_pop.length < nbr ) {

        if ( population.length > 0) {
            let rnd = Math.floor(rand.f64() * population.length)
            let path = population[rnd]

            const prob = (eval_path(path,cout)-min)/(max-min)
            rnd = rand.f64()
            if (rnd<prob)  new_pop.push(population.splice(rnd, 1)[0])

        }
        if( children.length > 0) {
            let rnd = Math.floor(Math.random() * children.length)
            let path = children[rnd]

            const prob = (eval_path(path,cout)-min)/(max-min)
            rnd = rand.f64()

            if (rnd<prob) new_pop.push(children.splice(rnd, 1)[0])
        }
    }

    population = new_pop

    for (const path of population) {
        const val =eval_path(path, cout)
        if ( val == min && val < best.val) {
            console.log("better")
            best.val = val
            best.path = path
        }
    }

    //console.log('tour'+i)
    if (population.length == 0) Deno.exit(2)


}

console.log(best.val)
console.log(best.path)