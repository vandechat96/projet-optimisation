let instances:number[][] =[]
for (let i = 1; i <= 10; i++) {
    const text = await Deno.readTextFile("../instances/data"+i+".dat");
    const tab = text.split("\r\n").map(Number)
    tab.pop()
    instances.push(tab);
}

function gen_base(instance:number[]):number[][] {

    const length = instance[0]
    const box = instance[2]
    const size = instance[1]
    let data:number[] = instance.splice(3,length+3)
    data.sort((a,b)=> -a + b)
    const space = box*size

    let div = 2
    while ((div+1) * length <= space){
        div+=1
    }
    let jsp = space - div*length

    let slice:number[][] = [...new Array(length)].map(()=>[])
    let i = 0
    let remaining_space = space

    while ( remaining_space > 0){
        if (i >= length) {
            i = 0
            remaining_space += div*jsp
            for (let j = 0; j < jsp; j++) {
                slice[j] = []
            }
            div = div+1
        }
        let coupe = Math.floor(data[i] / div)
        let reste = data[i] % div
        slice[i].push(coupe+reste)
        for (let j = 1; j < div; j++) {
            slice[i].push(coupe)
        }

        remaining_space -= div
        i++
    }

    return slice
}

function get_cost(param:number[],slice:number[][]):number{
    const length = param[0]
    const box = param[2]
    const size = param[1]

    if (slice.length < length) return -1
    else {
        let cost = 0
        let size_r = size
        let box_r = box
        for (let i = 0; i <length; i++) {
            for (let j = 0; j < slice[i].length; j++) {
                if (size_r === size){
                    cost += slice[i][j]
                    box_r--
                } else if (size_r <= 1) {
                    size_r = size+1
                }
                size_r--
            }
        }
        return box_r===0?cost:-1
    }
}


let instance = instances[9]
console.log(instance)
const result = gen_base(instance)
console.log(result)
console.log(get_cost(instance.splice(0,3),result))

