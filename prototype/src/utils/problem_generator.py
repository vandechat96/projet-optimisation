from random import randint


class Problem:

    def __init__(self, data, boundary):
        self.data = data
        self.boundary = boundary


def generate(min_value=1000, max_value=20000,
             min_count=10, max_count=50,
             min_boxes=3, max_boxes=10,
             min_cuts=10, max_cuts=25):
    """
    Outputs a tuple containing the generated problem
    as well as it's theoretical lower boundary
    """

    n = randint(min_count, max_count)
    b = randint(min_boxes, max_boxes)
    e = randint(min_cuts, max_cuts)

    output = [n, e, b]

    total = 0
    for n in range(n):
        num = randint(min_value, max_value)
        output.append(num)
        total += num

    lower_bound = total / e

    return Problem(output, lower_bound)


def save_problem(problem, name, path="../../custom_instances/"):
    EXTENSION = ".dat"
    with open(path + name + EXTENSION, "w") as file:
        file.write("\n".join(str(line) for line in problem.data))


def save_multiple(problems, name_base, path="../../custom_instances/"):
    for n in range(len(problems)):
        save_problem(generate(), f"{name_base}{(n + 1)}", path)


if __name__ == "__main__":
    problems = []
    for n in range(10):
        problems.append(generate())

    save_multiple(problems, "test")
