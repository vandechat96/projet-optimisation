import functools
import math
import random


def load_instances():
    instances = []
    for i in range(1, 11):
        sub = []
        f = open("../instances/data" + str(i) + ".dat", "r")
        lines = f.readlines()
        for line in lines:
            line = line.replace("\n", "")
            sub.append(int(line))
        instances.append(sub)
    return instances


# noinspection PyShadowingNames
def gen_base(instance, sort=True):
    length = instance[0]
    box = instance[2]
    size = instance[1]
    data = instance[3: length + 3]
    if sort:
        data.sort(reverse=True)
    space = box * size

    div = 1
    while (div + 1) * length <= space:
        div += 1

    jsp = space - div * length

    slice = [[] for i in range(length)]
    i = 0
    remaining_space = space

    while remaining_space > 0:
        if i >= length:
            i = 0
            remaining_space += div * jsp
            for j in range(jsp):
                slice[j] = []

            div = div + 1

        coupe = math.floor(data[i] / div)
        reste = data[i] % div
        slice[i].append(coupe + reste)
        for j in range(1, div):
            slice[i].append(coupe)

        remaining_space -= div
        i += 1

    return slice


def get_cost(param, slice):
    length = param[0]
    box = param[2]
    size = param[1]

    list_ = []
    for i in range(length):
        for j in range(len(slice[i])):
            list_.append(slice[i][j])
    list_.sort(reverse=True)

    if len(slice) < length:
        return -1
    else:
        cost = 0
        size_r = size
        box_r = box
        for i in range(len(list_)):
            if size_r == size:
                cost += list_[i]
                box_r -= 1
            elif size_r <= 1:
                size_r = size + 1
            size_r -= 1

        return cost if box_r == 0 else -1


# noinspection PyShadowingNames
def split_number_flat(number, split):
    nbr = []
    div = number // split
    surplus = number % split
    nbr.append(div + (surplus % split))
    for n in range(split - 1):
        nbr.append(div + surplus // split)

    return nbr


# noinspection PyShadowingNames
def split_number_random(number, split, randomness=0.4):
    nbr = []
    for n in range(split - 1):
        tranche = random.randint(math.floor(number * randomness), number)
        number = number - tranche
        nbr.append(tranche)

    nbr.append(number)

    return nbr


def space_occupied(solution):
    return functools.reduce(lambda a, b: a+len(b) if type(a) is int else len(a)+len(b), solution)


# noinspection PyShadowingNames
def show_box(param, result):
    size = param[1]

    list_ = []
    for i in range(len(result)):
        for j in range(len(result[i])):
            list_.append(result[i][j])
    list_.sort(reverse=True)

    space_left = size
    print("[ ", end="")
    for i in range(len(list_)):
        if space_left > 1:
            print(str(list_[i]) + ", ", end="")
            space_left -= 1
        else:
            print(str(list_[i]) + " ]")
            space_left = size
            if i < len(list_) - 1:
                print("[ ", end="")


if __name__ == '__main__':
    instance = load_instances()[0]
    print(instance)
    result = gen_base(instance)
    print(result)
    val = instance[0:3]
    print(get_cost(val, result))
    show_box(val, result)

'''
    test = [[3000, 3000, 1660],
            [3000, 3000, 1290],
            [3000, 2000, 1040, 1000],
            [2500, 2500, 1000, 890],
            [2500, 2500, 860],
            [2000, 2000, 1090],
            [2000, 2000, 640],
            [2000, 1000, 830],
            [2000, 1000, 630],
            [580]
            ]

    print(get_cost([10, 3, 10], test))
    show_box([10, 3, 10], test)'''
