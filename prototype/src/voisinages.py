import copy
from math import ceil, floor
import random as rnd

from prototype.src.utils.utils import *


def two_random_nbr(result_, method):
    result = copy.deepcopy(result_)
    length = len(result)

    indice1 = rnd.randint(0, length - 1)
    division1 = result[indice1]
    indice2 = rnd.choice([i for i in range(length - 1) if i not in [indice1]])
    division2 = result[indice2]

    nbr_box = len(division1) + len(division2)
    nbr1 = sum(division1)
    nbr2 = sum(division2)

    nbr_division = rnd.randint(1, nbr_box - 1)

    result[indice1] = method(nbr1, nbr_division)

    result[indice2] = method(nbr2, nbr_box - nbr_division)

    return result


# noinspection PyShadowingNames
def two_random_nbr_random(result_):

    return two_random_nbr(result_, split_number_random)


# noinspection PyShadowingNames
def two_random_nbr_not_random(result_):
    return two_random_nbr(result_, split_number_flat)


# noinspection PyShadowingNames
def coupage(param, result):
    length = param[0]

    list_ = [0 for i in range(length)]
    maxima = []
    result_ = [[] for i in range(length)]

    for i in range(len(result)):
        maxima.append(result[i][0])
        list_.append(sum(result[i]))

    for i in range(len(maxima)):
        boost = math.floor(maxima[i] * (rnd.random() / 5))
        if rnd.random() < 0.5:
            maxima[i] += boost
        else:
            maxima[i] -= boost

    for i in range(len(maxima)):
        for j in range(len(list_)):
            obj = maxima[i]
            while list_[j] - obj >= 0:
                result_[j].append(obj)
                list_[j] -= obj

    for i in range(len(list_)):
        for j in range(len(result_[i])):
            result_[i][j] += list_[i] // len(result_[i])
        list_[i] = 0

    return result_


'''
A neighbourhood searching method based on the maximum 
acceptable number to fit in a box instead of the 
previously used cut-based approach

In essence the new approach is based on the fact that
if the fist and the last element in a box are close to
each other, then the space in the box was used efficiently
and thus is close to an ideal situation, at least for an
average problem of the type we're working with
'''


def box_size_neighbourhood(problem, prev):
    cuts_per_box = problem[1]

    ''' 
    We can calculate a lower boundary for our solution by comparing
    the maximum total the boxes with the given target parameters can
    contain with the actual sum of all numbers in the initial problem 
    '''
    total_minimum = sum(problem[3:])
    total_prev = cuts_per_box * sum(prev)

    '''
    For a permutation we'll be targeting a random box
    with boundaries set so that the variation cannot go
    below the aforementioned minimum, with the upper boundary
    going up to (but not beyond) the size of the next bigger box

    This is made so that the box order never changes, saves us time
    on needing to sort the new solution each time, at the cost of
    potentially having a narrower immediate neighbourhood

    For now, let's keep the upper boundary for the 
    biggest box at 25% of it's initial size
    '''

    delta = 0
    box = 0
    # Prevents infinite loops by virtue of having multiple boxes of the same size
    while delta == 0:
        box = rnd.randint(0, len(prev) - 1)
        boundary = prev[box + 1] - prev[box] if box < problem[2] - 1 else -prev[box]
        lower_bound = max((total_minimum - total_prev) // cuts_per_box, boundary)
        # upper_bound = ceil(prev[0]*0.25) if box == 0 else prev[box-1]-prev[box]
        upper_bound = ceil(prev[0] * 5) if box == 0 else prev[box - 1] - prev[box]
        if lower_bound < upper_bound:
            delta = rnd.randint(lower_bound, upper_bound)

    res = copy.copy(prev)
    res[box] += delta

    return res


'''
A less restrained variation of box_size_unbounded
'''


def box_size_neighbourhood_unbounded(problem, prev):
    cuts_per_box = problem[1]

    total_minimum = sum(problem[3:])
    total_prev = cuts_per_box * sum(prev)

    delta = 0
    box = 0
    # Prevents infinite loops by virtue of having multiple boxes of the same size
    while delta == 0:
        box = rnd.randint(0, len(prev) - 1)
        lower_bound = max((total_minimum - total_prev) // cuts_per_box, -floor(prev[box] * 0.5))
        upper_bound = ceil(prev[box] * 0.5)
        if lower_bound < upper_bound:
            delta = rnd.randint(lower_bound, upper_bound)

    res = copy.copy(prev)
    res[box] += delta
    res.sort(reverse=True)

    return res


if __name__ == '__main__':
    instance = load_instances()[2]
    val = instance[0:3]
    print(instance[3:])

    result = gen_base(instance)
    print(result)
    # show_box(val, result)
    print(get_cost(val, result))

    voisin = coupage(val, result)
    # voisin = two_random_nbr_random(result)
    # voisin = two_random_nbr_random(result)
    # voisin = two_random_nbr_flat(result)
    # print(get_cost(val, voisin))

    print(voisin)
    print(get_cost(val, voisin))
    show_box(val, voisin)
