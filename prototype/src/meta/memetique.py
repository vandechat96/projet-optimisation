import functools

from prototype.src.voisinages import *


# noinspection PyShadowingNames
def generate_population(instance, size):
    population = []
    for i in range(size):
        population.append(gen_base(instance))
    return population


# noinspection PyShadowingNames
def generate_population_randomized(instance, size, neighbor, randomization):
    population = []
    for i in range(size):
        baby = gen_base(instance)
        for j in range(randomization):
            baby = neighbor(baby)
        population.append(baby)
    return population


def evaluate_cost(parameters, population):
    costs = []
    minimum = maximum = get_cost(parameters, population[0])
    best = population[0]

    for people in population:
        cost = get_cost(parameters, people)
        costs.append(cost)
        if cost < minimum:
            minimum = cost
            best = people

        maximum = max(maximum, cost)
    if maximum == minimum:
        maximum += 1
    return costs, minimum, maximum, best


# noinspection PyShadowingNames
def evolve(instance, population_number, number_of_generation, number_mutation, split_method, mutation_method):
    parameters = instance[:3]
    population = generate_population_randomized(instance, population_number, mutation_method, 3)

    best = population[0]

    for gen_nbr in range(number_of_generation):
        costs, min_cost, max_cost, best_gen = evaluate_cost(parameters, population)

        for people in population:
            if get_cost(parameters, people) == -1:
                print("error")
                exit(1)

        # Generate couples of parents
        parents = []
        tour = 0
        while len(parents) < population_number // 2:
            couple = []
            for i in range(len(population)):
                probability = (costs[i] - min_cost) / (max_cost - min_cost)
                random = rnd.random()

                if probability < random:
                    length = len(couple)
                    if length == 0:
                        couple.append(population[i])
                    else:
                        parents.append((couple[0], population[i]))
                        couple = []
                if len(parents) >= population_number // 2:
                    break
            if tour >= 1000:
                print("merde")
                exit(1)
        # Generate and mutate children
        children = []
        for parent1, parent2 in parents:
            random = rnd.random()

            child = copy.deepcopy(parent1)
            if random < 0.5:
                nbr1 = rnd.randint(0, len(parent1) - 1)
                nbr2 = rnd.randint(0, len(parent1) - 1)

                child[nbr1] = split_method(sum(child[nbr1]), len(parent2[nbr1]))
                child[nbr2] = split_method(sum(child[nbr2]), len(parent2[nbr2]))
                # child[nbr1] = parent2[nbr1]
                # child[nbr2] = parent2[nbr2]

                compensation = space_occupied(parent1) - space_occupied(child)
                derivation = 1 if compensation > 0 else -1
                tour = 0
                while space_occupied(child) != space_occupied(parent1):
                    for i in range(len(parent1) - 1):
                        if i not in [nbr1, nbr2]:
                            new_div = len(child[i]) + derivation
                            if new_div > 0:
                                child[i] = split_method(sum(child[i]), new_div)
                        if space_occupied(child) == space_occupied(parent1):
                            break
                    tour += 1
                    if tour >= 1000:
                        print("merde")
                        exit(1)

                old_child = child
                old_cost = get_cost(parameters, child)
                if rnd.random() < 0.5:
                    for i in range(number_mutation):
                        new_child = mutation_method(old_child)
                        new_cost = get_cost(parameters, new_child)
                        if new_cost < old_cost:
                            old_cost = new_cost
                            child = new_child
                children.append(child)

        population += children
        costs, min_cost, max_cost, best_gen = evaluate_cost(parameters, population)

        new_pop = []
        while len(new_pop) < population_number:
            i = rnd.randint(0, len(population) - 1)
            probability = (costs[i] - min_cost) / (max_cost - min_cost)
            random = rnd.random()

            if probability < random:
                new_pop.append(population[i])
                population.pop(i)

        population = new_pop
        if get_cost(parameters, best_gen) < get_cost(parameters, best):
            best = best_gen
    return best


if __name__ == '__main__':
    '''
        instances = load_instances()
    instance = instances[0]
    param = instance[:3]

    opti = evolve(instance, 20, 10)

    print(opti)
    print(get_cost(param, opti))
    '''

    prof_costs = [5243, 8190, 3897, 9978, 4966, 15030, 7194, 239778, 229428, 226788]

    for i in range(10):
        instance = load_instances()[i]
        param = instance[0:3]

        base = get_cost(param, gen_base(instance))

        solution = evolve(instance, 160, 400, 20, split_number_flat, two_random_nbr_not_random)
        cost = get_cost(param, solution)

        print(f"Problem number: {i + 1}")
        print(f"Variation related to base solution: {100 * (cost - base) / base} %")
        print(f"Variation related to the 'good' results: {100 * (cost - prof_costs[i]) / prof_costs[i]} %")
        # print(f"Final solution: {cost}, base solution {base}, 'good' solution: {prof_costs[i]}")
        # print(solution)

    print("====================== END ===========================")
