import time

from prototype.src.utils.utils import *
from prototype.src.voisinages import two_random_nbr_random, two_random_nbr_not_random
import time
import random
"""
Parameters:
    instance : array_like
        an instance
    neighborhood : function
        a function which compute a neighbor of a solution s
"""
"""la recherche locale consiste à rechercher le minimum local parmi un voisinage  """
def localSearch(instance, s, neighborhood, neighbors):
    values=instance[0:3]
    #neighbors=[] #contient les voisins rencontrés
    for j in range(len(neighbors)):
        found = True
        sprime = neighbors[j]
        if(get_cost(values,s)>get_cost(values,sprime)):
            s=sprime
        for i in range (len(neighbors)):
             if get_cost(values,neighbors[i])<get_cost(values,sprime):
                found = False
                break
        if found:
            break
        #neighbors.append(sprime)
    return s

"""
Parameters:
    instance : array_like
        an instance
    neighborhoods: array_like
        a list of neighborhoods
    neighbor : function
        a function which compute a neighbor of a solution s
"""

def VNS(instance, neighborhoods, neighbor):
    best = s = gen_base(instance)
    values=instance[0:3]
    kmax=len(neighborhoods)
    timeout = time.time() + 60
    #while True:
    k = 0
    while(k<kmax):
        rnd=random.randint(0,len(neighborhoods[k])-1)
        sprime=neighborhoods[k][rnd]
        ssec=localSearch(instance, sprime, neighbor, neighborhoods[k])
        if(get_cost(values,ssec)<get_cost(values,best)):
            s=ssec
        else:
            k=k+1
        best = s
        #if(timeout<time.time()):
            #break
    return best

if __name__ == '__main__':


    prof_costs = [5243, 8190, 3897, 9978, 4966, 15030, 7194, 239778, 229428, 226788]

    for i in range(10):
        init_time = time.time()
        instance = load_instances()[i]
        param = instance[0:3]

        base = get_cost(param, gen_base(instance))
        # two_random_nbr_random two_random_nbr_not_random
        neighborhoods=[]
        for x in range(700):
            neighbors = []
            neighbor=gen_base(instance)
            for j in range(700):
                neighbor=two_random_nbr_not_random(neighbor)
                neighbors.append(neighbor)
            neighborhoods.append(neighbors)
        solution =VNS(instance, neighborhoods, two_random_nbr_not_random)

        cost = get_cost(param, solution)

        endtime=time.time()
        print(f"\n\nProblem number: {i+1}")
        print(f"Variation related to base solution: {100 * (cost - base) / base} %")
        print(f"Variation related to the 'good' results: {100 * (cost - prof_costs[i]) / prof_costs[i]} %")
        print(f"Final solution: {cost}, base solution {base}, 'good' solution: {prof_costs[i]}")
        print(solution)
        print('sec = ' + str(endtime-init_time))

        print("====================== END ===========================")