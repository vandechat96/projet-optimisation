import time
import copy

from prototype.src.utils.utils import *
from prototype.src.voisinages import *

"""With Instance being the Instance of Values, voisinage the Neighborhood, k the size of the TabuList and niter the 
number of iterations. """


# noinspection PyShadowingNames
def tabu(instance, voisinage, k, niter):
    s0 = s = sstar = gen_base(instance)
    values = instance[0:3]
    best_cost = get_cost(values, s0)
    tabu_list = []

    timeout = time.time() + 60

    for i in range(niter):
        voisin = voisinage(sstar)
        cost = get_cost(values, voisin)
        if voisin not in tabu_list:
            if len(tabu_list) < k:
                if cost < best_cost:
                    tabu_list.append(sstar)
                    best_cost = cost
                    sstar = copy.deepcopy(voisin)
                else:
                    tabu_list.append(sstar)
            else:
                tabu_list.pop(0)
                if cost < best_cost:
                    tabu_list.append(sstar)
                    best_cost = cost
                    sstar = copy.deepcopy(voisin)
                else:
                    tabu_list.append(sstar)
    return sstar


if __name__ == '__main__':
    prof_costs = [5243, 8190, 3897, 9978, 4966, 15030, 7194, 239778, 229428, 226788]

    for i in range(10):
        instance = load_instances()[i]
        # instance[2] -= 1
        param = instance[0:3]

        base = get_cost(param, gen_base(instance))
        # two_random_nbr_random two_random_nbr_not_random
        solution = tabu(instance, two_random_nbr_random, 8, 100000)
        cost = get_cost(param, solution)

        print(f"\n\nProblem number: {i + 1}")
        print(f"Variation related to base solution: {100 * (cost - base) / base} %")
        print(f"Variation related to the 'good' results: {100 * (cost - prof_costs[i]) / prof_costs[i]} %")
        print(f"Final solution: {cost}, base solution {base}, 'good' solution: {prof_costs[i]}")
        print(solution)

    print("====================== END ===========================")
