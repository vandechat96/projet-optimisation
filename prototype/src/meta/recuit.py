from prototype.src.voisinages import *
import time


# noinspection PyShadowingNames
def recuit(instance, t0, tf, palier, coef, voisinage):
    previous = best = gen_base(instance)
    values = instance[0:3]
    previous_cost = best_cost = get_cost(values, previous)

    tc = t0
    # timeout = time.time() + 60 * 3

    while tc > tf:
        for i in range(palier):
            voisin = voisinage(previous)
            cost = get_cost(values, voisin)

            # xavier
            if cost < best_cost:
                best = copy.deepcopy(voisin)
                best_cost = cost

            delta_e = cost - previous_cost
            if delta_e <= 0 or rnd.random() < (math.exp(-delta_e / tc)):
                previous = voisin
                previous_cost = get_cost(values, previous)

        # if time.time() > timeout:
          #  break
        tc = tc * coef

    return best


if __name__ == '__main__':
    '''instance = load_instances()[5]
    val = instance[0:3]
    print(val)

    result = gen_base(instance)
    print(result)
    print(get_cost(val, result))

    opti = recuit(instance, 1000, 0.0001, 1000, 0.8, two_random_nbr_not_random)
    print(get_cost(val, opti))
    print(opti)
    #show_box(val, opti)
'''
    prof_costs = [5243, 8190, 3897, 9978, 4966, 15030, 7194, 239778, 229428, 226788]

    for i in range(10):
        instance = load_instances()[i]
        # instance[2] -= 1
        param = instance[0:3]

        base = get_cost(param, gen_base(instance))
        # two_random_nbr_random two_random_nbr_not_random
        solution = recuit(instance, 1000, 0.0001, 1000, 0.8, two_random_nbr_not_random)
        cost = get_cost(param, solution)

        print(f"Problem number: {i + 1}")
        print(f"Variation related to base solution: {100 * (cost - base) / base} %")
        print(f"Variation related to the 'good' results: {100 * (cost - prof_costs[i]) / prof_costs[i]} %")
        # print(f"Final solution: {best_cost}, base solution {base}, 'good' solution: {prof_costs[i]}")
        # print(solution)

    print("====================== END ===========================")
