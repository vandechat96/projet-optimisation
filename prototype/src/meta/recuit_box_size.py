from prototype.src.voisinages import *
from prototype.src.utils.utils import *

# import time

'''
The continuation of the approach elaborated in the box_size_neighbourhood function
It uses the principles of Simulated Annealing (Recuit Simulé) with a separate
static algorithm for actually cutting the different numbers up to fit in boxes
'''

'''
The main algo for this approach

Parameters:
    problem - the list containing the data of the problem
    t0 - initial temperature
    tf - final temperature
    rate - temperature cooling rate
    iter - number of steps the algorithm should remain at the same temperature
    neighbourhood - function for producing a neighbour solution
'''


# noinspection PyShadowingNames
def annealing(problem, t0, tf, rate, iter, neighbourhood):
    tc = t0
    prev = gen_base(problem)
    current = best = get_cost(problem, prev)

    while tc > tf:
        for i in range(iter):
            neighbour = neighbourhood(problem, prev)
            new = get_cost(problem, neighbour)
            delta_e = new[1] - current[1]
            if new[1] < best[1]:
                best = new
            # -deltaE/tc < 200 is here because Python dislikes big numbers
            if delta_e > 0 or (-delta_e / tc < 200 and rnd.random() < (math.exp(-delta_e / tc))):
                prev = neighbour
                current = new
        tc *= rate
        # print(best)

    return best


'''
Special function for generating a base solution for the problem
'''


# noinspection PyShadowingNames
def gen_base(problem):
    # Current approach: all cuts of equal size, may need to change it up later
    cut = (sum(problem[3:])) // (problem[1] * problem[2]) + 1
    base = []
    for n in range(problem[2]):
        base.append(cut)

    return base


'''
Function for constructing the actual cut-based solution and calculating it's cost

'''


# noinspection PyShadowingNames
def get_cost(problem, solution):
    numbers = problem[3:]
    cuts_per_box = problem[1]
    box_count = problem[2]

    # Solution with cuts attributed to a number, list structure set up in advance
    cuts = []
    for n in range(len(numbers)):
        cuts.append([])

    # # Solution with cuts attributed to a box, list structure set up in advance
    # boxes = []      
    # for n in range(solution):
    #     cuts.append([])

    '''
    This part of the algorithm slices up the numbers to fit in the boxes
    The base principle is to loop trough the numbers and make appropriate 
    sized cuts until the box if filled
    
    Once the numbers become too small it will take the maximum available number each time

    At the end the excess that didn't initially fit into any of the boxes will be added to a ...?
    '''
    box_progress = 0
    for i in range(len(solution) - 1):
        # First cycle, cuts off as many values of a box maximum as possible
        box_max = solution[i]
        box_progress = 0
        current_number_pos = 0
        while box_progress < cuts_per_box and current_number_pos < len(numbers):
            if numbers[current_number_pos] >= box_max:
                numbers[current_number_pos] -= box_max
                cuts[current_number_pos].append(box_max)
                box_progress += 1
            else:
                current_number_pos += 1

        # Second cycle, only if not enough cuts were made
        # Takes the maximum of all remaining cuts and puts it into the box
        while box_progress < cuts_per_box:
            maximum = max(enumerate(numbers), key=lambda x: x[1])
            cuts[maximum[0]].append(maximum[1])
            numbers[maximum[0]] = 0
            box_progress += 1

    # noinspection PyShadowingNames
    def remainder_filler():
        for n in range(len(cuts)):
            if not cuts[n]:
                return n
        return max(enumerate(numbers), key=lambda x: x[1])[0]

    for n in range(problem[1]):
        filler = remainder_filler()
        cuts[filler].append(numbers[filler])
        numbers[filler] = 0
        box_progress += 1

    # Dealing with what's left stage, the idea is to spread it equally among all other cuts
    # for n in range(len(numbers)):
    #     if numbers[n] > 0:
    #         fraction = numbers[n]//len(cuts[n])
    #         for m in range(len(cuts[n])-1):
    #             cuts[n][m] += fraction
    #             numbers[n] -= fraction
    #         cuts[n][-1] += numbers[n]
    #         numbers[n] = 0

    for n in range(len(numbers)):
        if numbers[n] > 0:
            cuts[n][0] += numbers[n]
            numbers[n] = 0

    # evaluating the result
    flat_cuts = []
    for number in cuts:
        flat_cuts += number
    flat_cuts.sort(reverse=True)

    # print(len(flat_cuts) == problem[1]*problem[2])

    result = 0
    for n in range(box_count):
        result += flat_cuts[n * cuts_per_box]

    return cuts, result


if __name__ == '__main__':

    prof_costs = [5243, 8190, 3897, 9978, 4966, 15030, 7194, 239778, 229428, 226788]

    for i in range(10):
        problem = load_instances()[i]

        # minimum = sum(problem[3:])/problem[1]

        # print(f"\n\nProblem number: {i + 1}")
        # print(f"Lower theoretical bound: {minimum}, 'Good' solution: {prof_costs[i]}")
        # print(f"Best possible variation related to the 'good' results: {100 * (minimum - prof_costs[i]) / prof_costs[i]} %")


        base = get_cost(problem, gen_base(problem))

        solution = annealing(problem, 1000, 0.001, 0.85, 100, box_size_neighbourhood)

        print(f"\n\nProblem number: {i + 1}")
        print(f"Variation related to base solution: {100 * (solution[1] - base[1]) / base[1]} %")
        print(f"Variation related to the 'good' results: {100 * (solution[1] - prof_costs[i]) / prof_costs[i]} %")
        print(f"Final solution: {solution[1]}, base solution {base[1]}, 'good' solution: {prof_costs[i]}")
        print(solution)

        verif = []
        for n in solution[0]:
            verif.append(sum(n))
        print(f"\nThe numbers in the solution are correct: {verif == problem[3:]}")

        flat_cuts = []
        for number in solution[0]:
            flat_cuts += number
        flat_cuts.sort(reverse=True)
        print(f"The number of cuts in the solution is correct: {len(flat_cuts) == problem[1] * problem[2]}")

    print("====================== END ===========================")
