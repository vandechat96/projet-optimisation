from prototype.src.voisinages import *
import time


# noinspection PyShadowingNames
def bruteforce(instance, voisinage):
    solution = gen_base(instance)
    values = instance[0:3]
    best_cost = get_cost(values, solution)

    timeout = time.time() + 60
    voisin = voisinage(solution)

    while True:
        voisin = voisinage(voisin)
        cost = get_cost(values, solution)
        if cost < best_cost:
            print(cost)
            solution = voisin
            best_cost = get_cost(values, solution)
        if time.time() > timeout:
            break

    return solution


if __name__ == '__main__':
    instance = load_instances()[0]
    val = instance[0:3]

    result = gen_base(instance)
    print(result)
    print(get_cost(val, result))

    opti = bruteforce(instance, two_random_nbr_random)
    print(opti)
    print(get_cost(val, opti))
